<?php
Class Translate{
	
	
	/* 
	 *
	 * function for creat translate via API
	 *
	*/
	public function getTranslate($src_text, $src_lang, $dst_lang){
		$src_text 	= urlencode($src_text);
		$url = 'http://www.transltr.org/api/translate?text='.$src_text.'&to='.$dst_lang.'&from='.$src_lang;
		
		$data= file_get_contents($url);		
		$arr = json_decode($data);
		return $arr->translationText;
	}
	
	/* 
	 *
	 * this function creat parser for Messenger bot request
	 * ussage: dnt tl sk to en Ahoj svet! //creat translate from Slovak to English
	 *
	*/
	public function translateParser($answer){
		$data = $answer;
		$langData = explode(" ", $data);
		$src_lang 	=  $langData[2];
		$dst_lang 	=  $langData[4];
		
		$textData = explode("dnt tl $src_lang to $dst_lang ", $data);
		$src_text = $textData[1]; 
		
		return self::getTranslate($src_text, $src_lang, $dst_lang);
	}

}