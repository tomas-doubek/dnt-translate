<?php
include "autoload.php";
$translate = new Translate();


/* 
 *
 * custom implementation
 *
*/
$src_text = "Ahoj svet!"; //returns Hi World
$src_lang = "sk";
$dst_lang = "en";
$custom = $translate->getTranslate($src_text, $src_lang, $dst_lang);


/* 
 *
 * dnt bot implementation 
 *
*/
$answer = "dnt tl sk to en Ahoj svet!"; //returns Hi World
$bot = $translate->translateParser($answer);
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="DntTranslate Demo">
      <meta name="author" content="Tomas Doubek">
      <title>Dnt Translate Demo</title>
   </head>
   <body>
	   <?php echo $custom; ?>
	   <hr/>
	   <?php echo $custom; ?>
   </body>
</html>